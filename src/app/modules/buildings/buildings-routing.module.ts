import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildingsDashboardComponent } from './pages/buildings-dashboard/buildings-dashboard.component';
import { BuildingDetailsComponent } from './pages/building-details/building-details.component';
import { LayoutComponent } from '../shared/components/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: BuildingsDashboardComponent
      }
    ]
  },
  {
    path: 'details/:id',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: BuildingDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuildingsRoutingModule { }
