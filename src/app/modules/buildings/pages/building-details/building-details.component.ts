import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Fridges, Sensors } from 'src/app/core/repositories/models/app.model';
import { FridgesService } from 'src/app/core/repositories/fridges.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SensorsService } from 'src/app/core/repositories/sensors.service';


@Component({
  selector: 'app-building-details',
  templateUrl: './building-details.component.html',
  styleUrls: ['./building-details.component.scss']
})
export class BuildingDetailsComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  private fridges: Fridges[] = [];
  public sensors: Sensors[] = [];

  constructor(
    private readonly fridgesService: FridgesService,
    private readonly sensorsService: SensorsService,
    private route: ActivatedRoute
  ) { }

  public ngOnInit(): void {
    if (!this.subscription)
      this.subscription = new Subscription();

    this.subscription.add(
      this.route.params.subscribe(param => {
        console.log(param);
        //this.subscription.add(
        this.fridgesService.getDevicesByBuildingId(param.id).subscribe((data: Fridges[]) => {
          this.fridges = data;
          console.log(this.fridges);
        })
        //);
        //this.subscription.add(
        this.sensorsService.getSensorsByDeviceId(param.id).subscribe((data: Sensors[]) => {
          this.sensors = data;
          console.log(this.sensors);
        })

        //);

        // this.subscription.add(this.fridgesService.getAll().subscribe((data: Fridges[]) => {
        //   this.fridges = data;
        //   console.log(this.fridges);
        // }));

      })
    );

  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
