import { Component, OnInit, OnDestroy } from '@angular/core';
import { BuildingService } from 'src/app/core/repositories/building.service';
import { Subscription } from 'rxjs';
import { Building } from 'src/app/core/repositories/models/app.model';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-buildings-dashboard',
  templateUrl: './buildings-dashboard.component.html',
  styleUrls: ['./buildings-dashboard.component.scss']
})
export class BuildingsDashboardComponent implements OnInit, OnDestroy {
  type = 'doughnut';
  data = {
    labels: ["OK", "ATTENTION", "ERROR"],
    datasets: [
      {
        label: "My First dataset",
        data: [300, 50, 100],
        backgroundColor:["#29d5ff","#cfb5e5","#d21777"],
        borderWidth: [0, 0, 0, 0]
      }
    ],
  };
  options = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: 'left',
      align: 'left',
      labels: {
          fontColor: 'white',
          usePointStyle: true
      }
    }
  };

  private subscription: Subscription = new Subscription();
  private buildings: Building[] = [];


  constructor(
    private readonly buildingService: BuildingService,
    private router: Router

  ) { }

  public ngOnInit(): void {

    if (!this.subscription)
      this.subscription = new Subscription();

    this.subscription.add(this.buildingService.getAll().subscribe((data: Building[]) => {
      this.buildings = data;
      console.log(this.buildings);
      this.buildings.forEach(building => {
        const addressData = building.address.split(', ');

        building['street'] = addressData[0];
        building['town'] = addressData[1];
        building['country'] = addressData[2];
      });
    }));
  }

  public onBuildingClicked(building): void {
    this.router.navigate(["/buildings/details/1/", {id: building.id}]);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
