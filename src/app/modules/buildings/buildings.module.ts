import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { BuildingsRoutingModule } from './buildings-routing.module';
import { BuildingsDashboardComponent } from './pages/buildings-dashboard/buildings-dashboard.component';
import { BuildingDetailsComponent } from './pages/building-details/building-details.component';

const Components = [
  BuildingsDashboardComponent,
  BuildingDetailsComponent
];

@NgModule({
  declarations: [
    ...Components
  ],
  imports: [
    CommonModule,
    BuildingsRoutingModule,
    SharedModule
  ]
})
export class BuildingsModule { }
