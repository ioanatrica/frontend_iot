import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material';
import { ChartModule } from 'angular2-chartjs';
import { RouterModule } from '@angular/router';

import { CustomMaterialModule } from './custom-material/custom-material.module';

import { ProgressSpinnerComponent } from './components/progress-spinner/progress-spinner.component';
import { LayoutComponent } from './components/layout/layout.component';
import { AlertComponent } from './components/alert/alert.component';

const ExportedComponents = [
  ProgressSpinnerComponent,
  LayoutComponent,
  AlertComponent
];

@NgModule({
  declarations: [
    ...ExportedComponents
  ],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    ChartModule,
    CustomMaterialModule,
    RouterModule
  ],
  exports: [
    ...ExportedComponents,
    ChartModule,
    CustomMaterialModule
  ]
})
export class SharedModule { }
