import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/app/core/security/authentication.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit {
 public isMenuOpen: boolean = false;
  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    if (window.innerWidth > 960) {
      this.isMenuOpen = true;
    }
  }

  public logout(): void {
    this.authenticationService.logout();
  }
}
