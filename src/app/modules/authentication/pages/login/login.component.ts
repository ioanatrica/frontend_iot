import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, tap } from 'rxjs/operators';

import { AuthenticationService } from '../../../../core/security/authentication.service';
import { Role } from './../../../../core/repositories/models/role.model';

@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public loading: boolean = false;
  public submitted: boolean = false;
  public error: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  public ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  @HostListener('document:keydown.enter', ['$event']) onKeydownHandler(event: KeyboardEvent): void {
    this.onLogInClicked();
  }

  // convenience getter for easy access to form fields
  get formControl(): any {
    return this.loginForm.controls;
  }

  public onLogInClicked(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.formControl.username.value, this.formControl.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/buildings']);
        },
        error => {
          this.loginForm.controls.username.setErrors({ incorrect: true });
          this.loginForm.controls.password.setErrors({ incorrect: true });
        }
      );
  }
}
