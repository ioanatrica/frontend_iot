import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CustomMaterialModule } from '../shared/custom-material/custom-material.module';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './pages/login/login.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { ErrorComponent } from './pages/error/error.component';
import { RegisterComponent } from './pages/register/register/register.component';

const Components = [
  LoginComponent,
  ForgotPasswordComponent,
  ResetPasswordComponent,
  RegisterComponent
];

@NgModule({
  declarations: [
    ...Components,
    ErrorComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AuthRoutingModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
