import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { DevicesRoutingModule } from './devices-routing.module';
import { DevicesDashboardComponent } from './pages/devices-dashboard/devices-dashboard.component';
import { DeviceDetailsComponent } from './pages/device-details/device-details.component';

const Components = [
  DevicesDashboardComponent,
  DeviceDetailsComponent
];

@NgModule({
  declarations: [
    ...Components,
  ],
  imports: [
    CommonModule,
    DevicesRoutingModule,
    SharedModule
  ]
})
export class DevicesModule { }
