import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevicesDashboardComponent } from './pages/devices-dashboard/devices-dashboard.component';
import { DeviceDetailsComponent } from './pages/device-details/device-details.component';
import { LayoutComponent } from '../shared/components/layout/layout.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DevicesDashboardComponent
      }
    ]
  },
  {
    path: 'details/:id',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DeviceDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevicesRoutingModule { }
