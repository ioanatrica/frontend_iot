import { Component, OnInit } from '@angular/core';
import { SensorsService } from 'src/app/core/repositories/sensors.service';
import * as Chart from 'chart.js';
import { Sensors } from 'src/app/core/repositories/models/app.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss']
})
export class DeviceDetailsComponent implements OnInit {
  private subscription: Subscription = new Subscription();
  public sensors: Sensors = null;
  // public id = 1;
  // chart: any;

  dataTemperature: any;
  optionsTemperature: any;

  type1 = 'doughnut';
  data1 = {
    labels: ["HIGH"],
    datasets: [
      {
        label: "My First dataset",
        data: [90, 10],
        backgroundColor: ["#d21777", "#808080", "#d21777"],
        borderWidth: [0, 0, 0, 0]
      }
    ],
  };
  options1 = {
    responsive: true,
    maintainAspectRatio: false,
    circumference: 1 * Math.PI,
    rotation: 1 * Math.PI,
    cutoutPercentage: 55,
    tooltips: {
      callbacks: {
        title: function (tooltipItem, data) {
          return tooltipItem[0].xLabel;
        },
        label: function (tooltipItem, data) {
          var label = data['labels'][tooltipItem['index']];
          if (label) {
            return label + '%';
          }
        }
      }
    },
    legend: {
      display: true,
      position: 'bottom',
      align: 'left',
      labels: {
        fontColor: 'white',
        usePointStyle: true
      }
    }
  };

  type2 = 'doughnut';
  data2 = {
    labels: ["OK"],
    datasets: [
      {
        label: "My First dataset",
        data: [55, 45],
        backgroundColor: ["#29d5ff", "#808080", "#d21777"],
        borderWidth: [0, 0, 0, 0]
      }
    ],
  };
  options2 = {
    responsive: true,
    maintainAspectRatio: false,
    circumference: 1 * Math.PI,
    rotation: 1 * Math.PI,
    cutoutPercentage: 55,
    tooltips: {
      callbacks: {
        title: function (tooltipItem, data) {
          return tooltipItem[0].xLabel;
        },
        label: function (tooltipItem, data) {
          var label = data['labels'][tooltipItem['index']];
          if (label) {
            return label + '%';
          }
        }
      }
    },
    legend: {
      display: true,
      position: 'bottom',
      align: 'left',
      labels: {
        fontColor: 'white',
        usePointStyle: true
      }
    }
  };

  type3 = 'doughnut';
  data3 = {
    labels: ["OK"],
    datasets: [
      {
        label: "My First dataset",
        data: [55, 45],
        backgroundColor: ["#b7f1ff", "#808080", "#d21777"],
        borderWidth: [0, 0, 0, 0]
      }
    ],
  };
  options3 = {
    responsive: true,
    maintainAspectRatio: false,
    circumference: 1 * Math.PI,
    rotation: 1 * Math.PI,
    cutoutPercentage: 55,
    tooltips: {
      callbacks: {
        title: function (tooltipItem, data) {
          return tooltipItem[0].xLabel;
        },
        label: function (tooltipItem, data) {
          var label = data['labels'][tooltipItem['index']];
          if (label) {
            return label + '%';
          }
        }
      }
    },
    legend: {
      display: true,
      position: 'bottom',
      align: 'left',
      labels: {
        fontColor: 'white',
        usePointStyle: true
      }
    }
  };

  type = 'line';
  data = {
    labels: ["temp1", "temp2", "temp3", "temp4", "temp5", "temp6", "temp7", "temp8", "temp9"],
    datasets: [
      {
        label: "My First dataset",
        data: [31.27, 30.06, 29.85, 29.73, 29.58, 29.51, 29.45, 29.35, 29.29],
        borderColor: '#29d5ff',
        backgroundColor: '#d21777',
        lineTension: 0,
        pointBorderColor: '#d21777',
        labelColor: '#d21777',
        fontColor: '#29d5ff',
        fill: false
      }
    ],
  };
  options = {
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          display: true,
          drawBorder: true,
          color: '#29d5ff',
        },
        ticks: {
          fontColor: '#29d5ff',
        },
      }],
      yAxes: [{
        stacked: true,
        gridLines: {
          display: true,
          drawBorder: true,
          color: '#29d5ff',
        },
        ticks: {
          beginAtZero: true,
          max: 40,
          stepSize: 10,
          fontColor: '#29d5ff',

        },
        scaleLabel: {
          display: true
        },
        afterTickToLabelConversion: function (q) {
          for (var tick in q.ticks) {
            q.ticks[tick] += '\u00B0C';
          }
        }
      }]
    },

    legend: {
      display: false
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem) {
          return tooltipItem.yLabel;
        }
      }
    }

  };

  type4 = 'bar';
  data4 = {
    labels: ["hum1", "hum2", "hum3", "hum4", "hum5", "hum6", "hum7", "hum8", "hum9"],
    datasets: [
      {
        label: "My First dataset",
        data: [47.47, 50.75, 51.19, 52.55, 52.81, 52.40, 52.97, 52.33, 52.21, 52.75],
        borderColor: '#29d5ff',
        backgroundColor: '#29d5ff',
        lineTension: 0,
        pointBorderColor: '#29d5ff',
        labelColor: '29d5ff',
        fontColor: '#29d5ff',
        fill: false
      }
    ],
  };
  options4 = {
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          display: true,
          drawBorder: true,
          color: '#29d5ff',
        },
        ticks: {
          fontColor: '#29d5ff',
        },
      }],
      yAxes: [{
        stacked: true,
        gridLines: {
          display: true,
          drawBorder: true,
          color: '#29d5ff',
          labelMaxWidth: 100,
        },
        ticks: {
          min: 10,
          max: 80,
          stepSize: 10,
          fontColor: '#29d5ff',

        },
        callback: function (value) {
          return (value / this.max * 100).toFixed(0) + '%';
        },
        scaleLabel: {
          display: true
        },
      }]
    },

    legend: {
      display: false
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem) {
          return tooltipItem.yLabel;
        }
      }
    }

  };

  type5 = 'bar';
  data5 = {
    labels: ["light1", "light2", "light3", "light4", "light5", "light6", "light7", "light8", "light9"],
    datasets: [
      {
        label: "My First dataset",
        data: [151, 117, 150, 148, 150, 150, 148, 149, 147],
        borderColor: '#29d5ff',
        backgroundColor: '#29d5ff',
        lineTension: 0,
        pointBorderColor: '#29d5ff',
        labelColor: '29d5ff',
        fontColor: '#29d5ff',
        fill: false
      }
    ],
  };
  options5 = {
    responsive: true,
    scales: {
      xAxes: [{
        tickMarkLength: 5,
        barThickness: 6,
        maxBarThickness: 8,
        gridLines: {
          display: true,
          drawBorder: true,
          color: '#29d5ff',
        },
        ticks: {
          fontColor: '#29d5ff',
        },
      }],
      yAxes: [{
        stacked: true,
        gridLines: {
          display: true,
          drawBorder: true,
          color: '#29d5ff',

        },
        ticks: {
          min: 0,
          max: 270,
          stepSize: 10,
          fontColor: '#29d5ff',

        },
        callback: function (value) {
          return (value / this.max * 100).toFixed(0) + '%';
        },
        scaleLabel: {
          display: true
        },
      }]
    },

    legend: {
      display: false
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem) {
          return tooltipItem.yLabel;
        }
      }
    }

  };

  dataSource = [
    { time: '15:01', temperature1: 27.15, temperature2: '5-10 °C', status: 'High' },
    { time: '15:02', temperature1: 27.12, temperature2: '5-10 °C', status: 'High' },
    { time: '15:03', temperature1: 27.12, temperature2: '5-10 °C', status: 'High' },
    { time: '15:04', temperature1: 27.12, temperature2: '5-10 °C', status: 'High' },
    { time: '15:05', temperature1: 27.13, temperature2: '5-10 °C', status: 'High' },
    { time: '15:06', temperature1: 27.12, temperature2: '5-10 °C', status: 'High' },
    { time: '15:07', temperature1: 27.15, temperature2: '5-10 °C', status: 'High' },
  ];

  displayedColumns: string[] = ['time', 'temperature1', 'temperature2', 'status'];
  isTableView: boolean = false;




  constructor(
    private readonly sensorsService: SensorsService,
    private route: ActivatedRoute

  ) { }

  ngOnInit() {


  }
}
