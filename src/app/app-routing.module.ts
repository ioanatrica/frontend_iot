import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ErrorComponent } from './modules/authentication/pages/error/error.component';
import { AuthGuard } from './core/guards/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: '/account/login', pathMatch: 'full' },
  {
    path: 'account',
    loadChildren: './modules/authentication/auth.module#AuthModule'
  },
  {
    path: 'buildings',
    loadChildren: './modules/buildings/buildings.module#BuildingsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'fridges',
    loadChildren: './modules/devices/devices.module#DevicesModule',
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: '/not-found' },
  {
    path: 'not-found',
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
