import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { User } from '../repositories/models/user.model';
import { environment } from '../../../environments/environment';
import { Role } from '../repositories/models/role.model';
import { map } from 'rxjs/internal/operators/map';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public hasRole(role: Role): boolean {
    return this.currentUserValue.role === role;
  }

  public login(username: string, password: string): Observable<any> {
    return this.http.post<any>(`${environment.identityEndpoint}/api/identity/Account/Login`, { username, password })
      .pipe(
          map((user: any) => {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem('currentUser', JSON.stringify(user));
              this.currentUserSubject.next(user);
            }

            return user;
          })
      );
  }

  public logout(): void {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
