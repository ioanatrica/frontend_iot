import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { API_URLS, apiUrls } from './repositories/api-urls.constant';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './intereceptors/jwt_interceptor';
import { ErrorInterceptor } from './intereceptors/error.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: API_URLS, useValue: apiUrls },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    // { provide: HTTP_INTERCEPTORS, useClass: FakeBackendInterceptor, multi: true },
    // fakeBackendProvider
  ]
})
export class CoreModule { }
