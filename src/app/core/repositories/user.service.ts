import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from './models/user.model'
import { environment } from './../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }

  public getById(id: number): Observable<any> {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
  }
}
