import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import { Building } from './models/app.model';
import { ApiCallObject } from './models/api.model';

@Injectable({
  providedIn: 'root'
})
export class BuildingService {
  constructor(
    private api: ApiService
  ) { }

  // Use this if you need to use a different api endpoint than the one used inside ./api.service.ts
  get baseUrl(): string {
    return environment.logisticsEndpoint;
  }

  public getAll(): Observable<Building[]> {
    return this.api.get<Building[]>(
      new ApiCallObject(
        this.baseUrl,
        'LOGISTICS.buildings'
      )
    );
  }
}
