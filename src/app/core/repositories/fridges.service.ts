import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import { Fridges } from './models/app.model';
import { ApiCallObject } from './models/api.model';

@Injectable({
  providedIn: 'root'
})
export class FridgesService {
  constructor(
    private api: ApiService
  ) { }

  // Use this if you need to use a different api endpoint than the one used inside ./api.service.ts
  get baseUrl(): string {
    return environment.logisticsEndpoint;
  }

  public getAll(): Observable<Fridges[]> {
    return this.api.get<Fridges[]>(
      new ApiCallObject(
        this.baseUrl,
        'LOGISTICS.fridges'
      )
    );
  }

  
  public getDevicesByBuildingId(id: number): Observable<Fridges[]> {
    return this.api.get<Fridges[]>(
      new ApiCallObject(
        this.baseUrl,
        'LOGISTICS.fridgesByBuildingId',
        { buildingId: id }
      )
    );
  }

}
