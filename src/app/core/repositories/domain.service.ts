import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { ApiResponse, ApiCallObject } from './models/api.model';
import { ApiService } from './api.service';
import { CultureDomain, Domain } from './models/app.model';

@Injectable({
  providedIn: 'root'
})
export class DomainService {
  constructor(
    private api: ApiService
  ) { }

  // Use this if you need to use a different api endpoint than the one used inside ./api.service.ts
  get baseUrl(): string {
    return environment.apiEndPoint;
  }

  public getDomainByKGId(KGId: number): Observable<ApiResponse<Domain>> {
    return null
  }

  public addCulturesToDomain(domainId: number, cultures: CultureDomain[]): Observable<ApiResponse<boolean>> {
    return null;
  }
}
