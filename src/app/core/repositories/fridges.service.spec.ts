import { TestBed } from '@angular/core/testing';

import { FridgesService } from './fridges.service';

describe('FridgesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FridgesService = TestBed.get(FridgesService);
    expect(service).toBeTruthy();
  });
});
