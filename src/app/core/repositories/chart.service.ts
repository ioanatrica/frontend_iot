import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import { Sensors } from './models/app.model';
import { Observable } from 'rxjs';
import { ApiCallObject } from './models/api.model';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(
    private api: ApiService
  ) { }
}
