import { InjectionToken } from '@angular/core';
export const API_URLS = new InjectionToken('apiUrls');

export interface IApiUrl {
  [key: string]: IApiUrls;
}

export interface IApiUrls {
  url?: string;
  children?: IApiUrl;
}

export const apiUrls: IApiUrls = {
  children: {
    LOGISTICS: {
      url: 'logistics',
      children: {
        buildings: { url: 'Buildings' },
        fridges: { url: 'Device' },
        fridgesByBuildingId: { url: 'Device/building/{buildingId}' }
      }
    },
    SENSORS: {
      url: 'sensors',
      children: {
        fridges: { url: 'Device' },
        sensorsByDeviceId: { url: 'Adapter/{deviceId}' }
      }
    },
    SAT: {
      url: 'SAT',
      children: {
        domains: { url: 'domains' },
        cultureDomains: { url: 'domains/{domainId}/cultures' }
      }
    },
    FAKE: {
      url: '',
      children: {
        auth: { url: '/users/authenticate' }
      }
    }
  }
};
