import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import { Sensors } from './models/app.model';
import { Observable } from 'rxjs';
import { ApiCallObject } from './models/api.model';

@Injectable({
  providedIn: 'root'
})
export class SensorsService {

  constructor(
    private api: ApiService
  ) { }

  get baseUrl(): string {
    return environment.sensorsEndpoint;
  }

  public getAll(): Observable<Sensors[]> {
    return this.api.get<Sensors[]>(
      new ApiCallObject(
        this.baseUrl,
        'SENSORS.sensors'
      )
    );
  }

  public getSensorsByDeviceId(id: number): Observable<Sensors[]> {
    return this.api.get<Sensors[]>(
      new ApiCallObject(
        this.baseUrl,
        'SENSORS.sensorsByDeviceId',
        { deviceId: id }
      )
    );
  }
}
