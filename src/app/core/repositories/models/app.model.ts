export class CultureDomain {
  public id: number;
  public isCultureDefault: boolean;
}

export class Domain {
  public id: number;
  public name: string;
  public kgId: number;
}

export class User {
  userId: number;
  firstName: string;
  lastName: string;
  jobTitle: string;
  userName: string;
  mail: string;
  roleId: number;
  roleName: string;
  token: string;
}

export class Building {
  name: string;
  address: string;
  lowFloor: number;
  highFloor: number;
  countryCode: string;
  userId: string;
}

export class Fridges {
  id: number;
  name: string;
  data: number;
  status: number;
  buildingsId: number;
  deviceTypeId: number;
}

export class Sensors {
  id: number;
  deviceId: string;
  temperature: number;
  humidity: number;
  light: number;
  isDoorOpened: true;
  dateTime: string;
}
