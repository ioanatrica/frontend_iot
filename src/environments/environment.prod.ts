export const environment = {
  production: true,
  apiEndPoint: 'https://amwebapi-dev.azurewebsites.net/',
  apiUrl: 'https://localhost:4200',
  identityEndpoint: 'https://licenta-iot-gateway.azurewebsites.net',
  logisticsEndpoint: 'https://licenta-iot-gateway.azurewebsites.net/api/',
  sensorsEndpoint : 'https://licenta-iot-gateway.azurewebsites.net/api/sensors/'
};
